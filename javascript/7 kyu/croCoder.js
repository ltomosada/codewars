// Square the value of every element in the array. Presume that you will only get numbers in the input array

function arraySquared(array) {
  return array.map((x) => Math.pow(x, 2));
}

console.log(arraySquared([1, 2, 3, 4, 5]));

function sumOfEveryPosEl(array) {
  return array.filter((x) => x > 0).reduce((sum, num) => sum + num, 0);
}

console.log(sumOfEveryPosEl([1, -4, 12, 0, -3, 29, -150]));

function calculateMean(array) {
  return array.reduce((sum, num) => sum + num, 0) / array.length;
}

function calculateMedian(array) {
  return array.sort(function (a, b) {
    return a - b;
  })[array.length / 2];
}

console.log(calculateMean([12, 46, 32, 64]));
console.log(calculateMedian([12, 46, 32, 64]));

function getNameInitials(name) {
  return name
    .split(" ")
    .map((x) => x[0])
    .join("");
}
console.log(getNameInitials("George Raymond Richard Martin"));

function ageDifference(input) {
  const ages = input.map((person) => person.age);
  return [
    Math.max(...ages),
    Math.min(...ages),
    Math.max(...ages) - Math.min(...ages),
  ];
}
const input = [
  {
    name: "John",
    age: 13,
  },
  {
    name: "Mark",
    age: 56,
  },
  {
    name: "Rachel",
    age: 45,
  },
  {
    name: "Nate",
    age: 67,
  },
  {
    name: "Jennifer",
    age: 65,
  },
];
console.log(ageDifference(input));

function numeronyms(input) {
  const words = input.split();
}
