// Take 2 strings s1 and s2 including only letters from a to z. Return a new sorted string, the longest possible, containing distinct letters - each taken only once - coming from s1 or s2.

// Examples:
// a = "xyaabbbccccdefww"
// b = "xxxxyyyyabklmopq"
// longest(a, b) -> "abcdefklmopqwxy"

// a = "abcdefghijklmnopqrstuvwxyz"
// longest(a, a) -> "abcdefghijklmnopqrstuvwxyz"

function longest(s1, s2) {
  let res = [];
  for (let c of s1) {
    if (res.includes(c)) {
      continue;
    } else {
      res.push(c);
    }
  }
  for (let c of s2) {
    if (res.includes(c)) {
      continue;
    } else {
      res.push(c);
    }
  }
  res = res.sort();

  return res.join("");
}

const longest = (s1, s2) => [...new Set(s1 + s2)].sort().join("");
