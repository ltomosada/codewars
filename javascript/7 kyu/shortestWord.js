// Simple, given a string of words, return the length of the shortest word(s).

// String will never be empty and you do not need to account for different data types.

function findShort(s) {
  s = s.split(" ");
  let len = s[0].length;
  for (let word of s) {
    if (word.length < len) {
      len = word.length;
    }
  }
  return len;
}
