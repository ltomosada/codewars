// Find the number with the most digits.

// If two numbers in the argument array have the same number of digits, return the first one in the array.

function findLongest(array) {
  // code here
  let longest = array[0].toString();
  for (let num of array) {
    if (num.toString().length > longest.length) {
      longest = num.toString();
    }
  }
  return Number(longest);
}

const findLongest = (l) =>
  l.reduce((a, b) => (`${b}`.length > `${a}`.length ? b : a));
